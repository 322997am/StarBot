#!/usr/bin/env python3

import aiosqlite
import os
import discord
from discord.ext import commands
from datetime import datetime
from starbotfunctions import update_records_API, add_reaction_DB, del_reaction_DB, update_board_message, del_board_message, gen_embed, bool_word
import re
import time
import math
import requests

botprefix = "star "
bot = commands.Bot(command_prefix=botprefix)



admins = [325699525928812544]


@bot.event
async def on_ready():
    print("Bot has connected")
    if(os.path.isfile("guilds.db") != True):
        async with aiosqlite.connect("guilds.db") as db:
            await db.execute('CREATE TABLE "guilds" ( "id" INTEGER, "guildid" INTEGER, "channelid" INTEGER, "emojiid" INTEGER, "emojiname" INTEGER, "starthreshold" INTEGER, nsfw INTEGER, selfstar INTEGER, botstar INTEGER, PRIMARY KEY("id" AUTOINCREMENT) )')
            await db.commit()
    if(os.path.isfile("messages.db") != True):
        async with aiosqlite.connect("messages.db") as db:
            await db.execute('CREATE TABLE "messages" ( "id" INTEGER, "guildid" INTEGER, "channelid" INTEGER, "messageid" INTEGER, "starcount" INTEGER, "boardchannelid" INTEGER, "boardmessageid" INTEGER, "userid" INTEGER, PRIMARY KEY("id" AUTOINCREMENT) )')
            await db.commit()
    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds")
        record = await record.fetchone()
        if(len(record) == 6):  # update guilds database to v2 version, add nsfw row and default to 0
            await db.execute("ALTER TABLE guilds ADD COLUMN nsfw INTEGER")
            await db.execute("UPDATE guilds SET nsfw=0")
            await db.commit()
        if(len(record) == 7):  # update guilds database to v3, add selfstar and botstar rows and default to 0
            await db.execute("ALTER TABLE guilds ADD COLUMN selfstar INTEGER")
            await db.execute("UPDATE guilds SET selfstar=0")
            await db.commit()
            await db.execute("ALTER TABLE guilds ADD COLUMN botstar INTEGER")
            await db.execute("UPDATE guilds SET botstar=0")
            await db.commit()


@bot.event
# called quite often so effort must be taken to ensure that the API is
# only called if the emoji matches
async def on_raw_reaction_add(rawreaction):
    if(rawreaction.emoji.is_custom_emoji()):
        emojiid = rawreaction.emoji.id
        emojiname = rawreaction.emoji.name
    else:
        emojiid = -1
        emojiname = rawreaction.emoji.name
    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (rawreaction.guild_id,))
        record = await record.fetchone()
    if(record is None):  # if guild isn't initialized, exit
        return
    # check that the records line up with the raw_reaction_add event
    if(record[3] == emojiid and record[4] == emojiname):
        guild = bot.get_guild(rawreaction.guild_id)
        mtuple = await update_records_API(guild, rawreaction.channel_id, rawreaction.message_id, record)
        starcount = mtuple[0]
        message = mtuple[1]
        # print(record[5])
        if(starcount >= record[5]):

            # print(starcount)
            # print(record[5])
            if(starcount < record[5]):
                return
            boardchannel = message.guild.get_channel(record[2])
            await update_board_message(boardchannel, starcount, message, emojiid, emojiname)


@bot.event
# inverse of on_raw_reaction_add(rawreaction):    mostly identical code,
# but reversed
async def on_raw_reaction_remove(rawreaction):
    if(rawreaction.emoji.is_custom_emoji()):
        emojiid = rawreaction.emoji.id
        emojiname = rawreaction.emoji.name
    else:
        emojiid = -1
        emojiname = rawreaction.emoji.name
    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (rawreaction.guild_id,))
        record = await record.fetchone()
    if(record is None):
        return
    if(record[3] == emojiid and record[4] == emojiname):
        # starcount = await delReactionDB(rawreaction.guild_id,
        # rawreaction.channel_id, rawreaction.message_id)
        guild = bot.get_guild(rawreaction.guild_id)
        # print(starcount)
        # print(record[5])
        mtuple = await update_records_API(guild, rawreaction.channel_id, rawreaction.message_id, record)
        starcount = mtuple[0]
        message = mtuple[1]

        # mtuple = await update_records_API(guild, rawreaction.channel_id,
        # rawreaction.message_id, emojiid, emojiname)

        async with aiosqlite.connect("messages.db") as db:
            boardmessageid = await db.execute("SELECT * FROM messages WHERE messageid=?", (rawreaction.message_id,))
            boardmessageid = await boardmessageid.fetchone()
            boardchannel = guild.get_channel(boardmessageid[5])
        boardmessage = await boardchannel.fetch_message(boardmessageid[6])
        if(starcount < record[5]):
            await del_board_message(boardmessage)
        else:
            await update_board_message(boardchannel, starcount, message, emojiid, emojiname)


@bot.command()
async def init(ctx):  # guild initialization. Maybe add defaults in the future
    if(not ctx.author.guild_permissions.manage_guild):
        return
    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (ctx.guild.id,))
        record = await record.fetchone()
        # print(record)
        if(record is None):
            await db.execute("INSERT INTO guilds (guildid) VALUES (?)", (ctx.guild.id,))
            await db.commit()
            await ctx.send("Server initialization complete. \n Set the starboard channel with `setchannel`. \n Set the starboard threshold with `threshold`. \n Set the starboard emoji with `emoji`. Set the nsf state with `nsfw`. \n the prefix of the bot is `" + botprefix + "`.")
            return
        else:
            await ctx.send("Server already initialized")
            return


@bot.command()
async def setchannel(ctx, channellink):  # set starboard channel
    if(not ctx.author.guild_permissions.manage_guild):
        return
    channelid = re.search("<#([0-9]+)>", channellink).group(1)
    if channelid is None:
        await ctx.send("Formatting Error")
        return
    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (ctx.guild.id,))
        record = await record.fetchone()
        if(record is None):
            await ctx.send("Please initialize the server with `" + botprefix + "init`")
            return
        await db.execute("UPDATE guilds SET channelid=? WHERE guildid=?", (int(channelid), ctx.guild.id))
        await db.commit()
    await ctx.send("Updated Starboard Channel")


@bot.command()
async def threshold(ctx, number):  # set star threshold
    if(not ctx.author.guild_permissions.manage_guild):
        return
    try:
        int(number)
    except ValueError:
        await ctx.send("Formatting Error")
        return
    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (ctx.guild.id,))
        record = await record.fetchone()
        if(record is None):
            await ctx.send("Please initialize the server with `" + botprefix + "init`")
            return
        await db.execute("UPDATE guilds SET starthreshold=? WHERE guildid=?", (int(number), ctx.guild.id))
        await db.commit()
    await ctx.send("Updated Starboard Threshold")


@bot.command()
async def emoji(ctx, emojitext):  # set star emoji
    if(not ctx.author.guild_permissions.manage_guild):
        return
    if(re.search("<:(.*):([0-9]+)>", emojitext) is not None):
        emojiname = re.search("<:(.*):([0-9]+)>", emojitext).group(1)
        emojiid = int(re.search("<:(.*):([0-9]+)>", emojitext).group(2))
        # print(emojiid)
        # print(emojiname)
    else:
        # print(emojitext)
        emojiid = -1
        emojiname = emojitext
    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (ctx.guild.id,))
        record = await record.fetchone()
        if(record is None):
            await ctx.send("Please initialize the server with `" + botprefix + "init`")
            return
        await db.execute("UPDATE guilds SET emojiid=?, emojiname=? WHERE guildid=?", (emojiid, emojiname, ctx.guild.id))
        await db.commit()
    await ctx.send("Updated Star Emoji")


@bot.command()
# scrape through every past message on the starboard.
async def importoldstars(ctx):
    if(not ctx.author.guild_permissions.manage_guild):
        return
    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (ctx.guild.id,))
        record = await record.fetchone()
    if(record is not None):
        for value in record:
            if(value is None):
                await ctx.send("Please initialize the server with `" + botprefix + "init`, and/or set all necessary parameters")
                return
    else:
        await ctx.send("Please initialize the server with `" + botprefix + "init`")
        return
    await ctx.send("Importing old stars started.")
    starchannel = ctx.guild.get_channel(record[2])
    emojiid = record[3]
    emojiname = record[4]
    starthreshold = record[5]

    async for message in starchannel.history(limit=1):
        deadstarttime = message.created_at

    async for message in starchannel.history(limit=None, oldest_first=True):
        for embed in message.embeds:
            content = str(embed.to_dict())
            matches = re.search(
                r"https://(discord|discordapp)\.com/channels/([0-9]+)/([0-9]+)/([0-9]+)",
                content)  # regex for message link
            channelid = int(matches.group(3))
            messageid = int(matches.group(4))
            # print(messageid)
            # print(channelid)
            await update_records_API(ctx.guild, channelid, messageid, record)
    # print("m4")
    for channel in ctx.guild.channels:
        if("TextChannel" not in str(type(channel))):
            continue
        async for message in channel.history(limit=None, after=deadstarttime, oldest_first=True):
            mtuple = await update_records_API(ctx.guild, channel.id, message.id, record)
            if(mtuple[0] >= starthreshold):
                await update_board_message(starchannel, mtuple[0], mtuple[1], emojiid, emojiname)
    await ctx.send("Done importing old stars")


@bot.command()
async def importdeadzone(ctx, timearg):
    # this is for importing all messages from when the last starbot was
    # disabled, turned off, banned, broken, or otherwise not able to log
    # starboard messages
    timesince = datetime.strptime(timearg, "%Y-%m-%d")
    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (ctx.guild.id,))
        record = await record.fetchone()

    starchannel = ctx.guild.get_channel(record[2])
    emojiid = record[3]
    emojiname = record[4]
    starthreshold = record[5]
    await ctx.send("Began importing dead zone")
    for channel in ctx.guild.channels:
        if(channel.permissions_for(ctx.guild.me).read_messages != True):
            continue
        if("TextChannel" not in str(type(channel))):
            continue
        async for message in channel.history(limit=None, after=timesince, oldest_first=True):
            print(message.id)
            mtuple = await update_records_API(ctx.guild, channel.id, message.id, record)
            if(mtuple[0] >= starthreshold):
                await update_board_message(starchannel, mtuple[0], mtuple[1], emojiid, emojiname)
    await ctx.send("Done importing old stars")


@bot.command()
async def top(ctx, *args):
    number = 1
    for arg in args:
        number = int(arg)
    starcountlist = []
    async with aiosqlite.connect("messages.db") as db:
        userlist = await db.execute("SELECT DISTINCT userid FROM messages WHERE guildid=? ORDER BY userid", (ctx.guild.id,))
        userlist = await userlist.fetchall()
        for user in userlist:
            userstarcount = await db.execute("SELECT sum(starcount) FROM messages WHERE userid=? AND guildid=?", (user[0], ctx.guild.id))
            userstarcount = await userstarcount.fetchone()
            starcountlist.append([user[0], userstarcount[0]])
        # print(starcountlist)
    starcountlist = sorted(starcountlist, key=lambda x: x[1], reverse=True)
    embed = discord.Embed(description="Top Stars")
    pagecount = math.ceil(len(starcountlist) / 10)
    embed.color = discord.Colour(0xe6e600)
    embed.set_author(name="Page " + str(number) + "/" + str(pagecount))
    lowerbound = number * 10 - 10
    upperbound = number * 10
    if(upperbound > len(starcountlist)):
        upperbound = len(starcountlist)
    embedstring = ""
    for i in range(lowerbound, upperbound):
        print(starcountlist[i][0])
        try:
            member = await ctx.guild.fetch_member(starcountlist[i][0])
        except BaseException:
            embedstring += str(i + 1) + ". " + \
                str(starcountlist[i][0]) + " - " + str(starcountlist[i][1]) + "\n"
            continue
        embedstring += str(i + 1) + ". " + member.name + "#" + \
            str(member.discriminator) + " - " + str(starcountlist[i][1]) + "\n"
    embed.add_field(name="\u200b", value=embedstring, inline=False)
    place = 1
    for entry in starcountlist:
        if(entry[0] == ctx.author.id):
            break
        place += 1
    embed.set_footer(text="Your place is: " + str(place))
    await ctx.send(embed=embed)


@bot.command()
async def repair(ctx):  # go through DB and check that all messages line up.
    if(not ctx.author.guild_permissions.manage_guild):
        return
    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (ctx.guild.id,))
        record = await record.fetchone()
    if(record is None):
        return
    await ctx.send("Starting database repair")
    async with aiosqlite.connect("messages.db") as db:
        guildstarlist = await db.execute("SELECT * FROM messages WHERE guildid=?", (ctx.guild.id,))
        guildstarlist = await guildstarlist.fetchall()
    for starentry in guildstarlist:
        exitcode = await update_records_API(ctx.guild, starentry[2], starentry[3], record)
        # print(exitcode)


@bot.command()
# get top message. A regex is ran in order to extract ID from pings
async def topmessage(ctx, *args):
    guildid = ctx.guild.id
    userid = -1
    # print(args)
    for arg in args:
        try:
            userid = int(arg)
            continue
        except ValueError:
            pass
        try:
            #print(re.search("<@!([0-9]+)>", arg).group(1))
            userid = int(re.search("<@!([0-9]+)>", arg).group(1)) # group(1) failes with AttributeError if no regex exists, which is caught below
            continue
        except AttributeError:
            pass
        if(arg == "me"):
            userid = ctx.author.id
            pass

    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (guildid,))
        record = await record.fetchone()
    if(record is None):
        return

    emojiid = record[3]
    emojiname = record[4]

    async with aiosqlite.connect("messages.db") as db:
        if(userid == -1):
            topstar = await db.execute("SELECT *, max(starcount) FROM messages WHERE guildid=?", (ctx.guild.id,))
            topstar = await topstar.fetchone()
        else:
            topstar = await db.execute("SELECT *, max(starcount) FROM messages WHERE guildid=? AND userid=?", (ctx.guild.id, userid))
            topstar = await topstar.fetchone()
    channel = ctx.guild.get_channel(topstar[2])
    message = await channel.fetch_message(topstar[3])
    starcount = topstar[4]
    if(emojiid == -1):
        boardmessagecontent = "**" + \
            str(starcount) + "** " + emojiname + "  |  " + message.channel.mention
    else:
        boardmessagecontent = "**" + \
            str(starcount) + "** <:" + emojiname + ":" + str(emojiid) + ">  |  " + message.channel.mention
    await ctx.send(boardmessagecontent, embed=gen_embed(message))


@bot.command()
async def nsfw(ctx, value):  # toggle nsfw starboarding
    if(not ctx.author.guild_permissions.manage_guild):
        return
    if(bool_word(value) == True):
        number = 1
    elif(bool_word(value) == False):
        number = 0
    else:
        await ctx.send("Unrecognized input")
        return

    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (ctx.guild.id,))
        record = await record.fetchone()
        if(record is None):
            await ctx.send("Please initialize the server with `" + botprefix + "init`")
            return
        await db.execute("UPDATE guilds SET nsfw=? WHERE guildid=?", (number, ctx.guild.id))
        await db.commit()
    if(number == 1):
        await ctx.send("Messages on NSFW channels will now be included on the starboard")
    else:
        await ctx.send("Messages on NSFW channels will no longer be included on the starboard")


@bot.command()
async def selfstar(ctx, value):  # toggle self starring
    if(not ctx.author.guild_permissions.manage_guild):
        return
    if(bool_word(value) == True):
        number = 1
    elif(bool_word(value) == False):
        number = 0
    else:
        await ctx.send("Unrecognized input")
        return

    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (ctx.guild.id,))
        record = await record.fetchone()
        if(record is None):
            await ctx.send("Please initialize the server with `" + botprefix + "init`")
            return
        await db.execute("UPDATE guilds SET selfstar=? WHERE guildid=?", (number, ctx.guild.id))
        await db.commit()
    if(number == 1):
        await ctx.send("Self-Starring enabled")
    else:
        await ctx.send("Self-Starring disabled")


@bot.command()
async def botstar(ctx, value):  # toggle bot starring
    if(not ctx.author.guild_permissions.manage_guild):
        return
    if(bool_word(value) == True):
        number = 1
    elif(bool_word(value) == False):
        number = 0
    else:
        await ctx.send("Unrecognized input")
        return

    async with aiosqlite.connect("guilds.db") as db:
        record = await db.execute("SELECT * FROM guilds WHERE guildid=?", (ctx.guild.id,))
        record = await record.fetchone()
        if(record is None):
            await ctx.send("Please initialize the server with `" + botprefix + "init`")
            return
        await db.execute("UPDATE guilds SET botstar=? WHERE guildid=?", (number, ctx.guild.id))
        await db.commit()
    if(number == 1):
        await ctx.send("Bot-Starring enabled")
    else:
        await ctx.send("Bot-Starring disabled")


@bot.command()
async def exportdb(ctx):
    if(not ctx.author.guild_permissions.manage_guild):
        return
    await ctx.send(file=discord.File(open("guilds.db", "rb")))
    await ctx.send(file=discord.File(open("messages.db", "rb")))

if(os.path.isfile(".token") != True):
    open(".token", 'a').close()
    print("Empty .token file created. The bot will not work until you get your own token from the discord developer portal.")

f = open(".token", "r")
token = f.read().split()[0]

state = 0
while(state == 0):  # wait until discord is online, then start bot.
    time.sleep(0.5)
    try:
        requests.get('https://discord.com')
        state = 1
        print("Connection to Discord available. Attempting to start bot.")
    except BaseException:
        state = 0


bot.run(token)
