import aiosqlite
import os
import discord
from discord.ext import commands
from datetime import datetime
import re

async def update_records_API(guild, channelid, messageid, guildrecord): #the main function used to update the records using the discord API. 
    emojiid = guildrecord[3]
    emojiname = guildrecord[4]
    nsfw = guildrecord[6]
    selfstar = guildrecord[7]
    botstar = guildrecord[8]
    channel = guild.get_channel(channelid)
    if(channel is None):
        return 0, None
    if(channel.is_nsfw() and nsfw == 0):
        return -1
    if(channel.permissions_for(guild.me).manage_messages != True or channel.permissions_for(guild.me).read_messages != True):
        #print('equisde')
        return -1
    try:
        message = await channel.fetch_message(messageid)
    except discord.errors.NotFound:
        return 0, None
    subtract = 0
    for reaction in message.reactions:
        #go through reactions and remove haram ones
        if(reaction.custom_emoji):
            if(emojiid == reaction.emoji.id and emojiname == reaction.emoji.name):
                async for user in reaction.users():
                    #print(user.name)
                    if(message.author.id == user.id and selfstar == 0): #remove self stars
                        await reaction.remove(user)
                        subtract = subtract + 1
                    if(user.bot and botstar == 0): #remove bot stars
                        await reaction.remove(user)
                        subtract = subtract + 1
                starcount = reaction.count
                break
        else:
            if(reaction.emoji == emojiname):
                async for user in reaction.users():
                    if(message.author.id == user.id and selfstar == 0):
                        await reaction.remove(user)
                        subtract = subtract + 1
                    if(user.bot and botstar == 0):
                        await reaction.remove(user)
                        subtract = subtract + 1
                starcount = reaction.count
                break
    try:
        testvar = starcount
    except UnboundLocalError:
        return 0, message
    starcount = starcount - subtract
    if(starcount == 0):
        return 0, message
    async with aiosqlite.connect("messages.db") as db: #update messages db
        existingrecord = await db.execute("SELECT * FROM messages WHERE messageid=?", (messageid, ))
        existingrecord = await existingrecord.fetchone()
        if existingrecord:
            await db.execute("UPDATE messages SET starcount=?, userid=? WHERE messageid=? ", (starcount, message.author.id, messageid))
            await db.commit()
            return starcount, message
        else:
            await db.execute("INSERT INTO messages (guildid, channelid, messageid, starcount, userid) VALUES (?, ?, ?, ?, ?)", (guild.id, channelid, messageid, starcount, message.author.id))
            await db.commit()
            return starcount, message      
            
async def add_reaction_DB(guildid, channelid, messageid, userid): #unused function
    async with aiosqlite.connect("messages.db") as db:
        existingrecord = await db.execute("SELECT * FROM messages WHERE messageid=?", (messageid, ))
        existingrecord = await existingrecord.fetchone()
        if existingrecord is not None:
            starcount = existingrecord[4] + 1
            #print("m2")
            await db.execute("UPDATE messages SET starcount=? WHERE messageid=? ", (starcount, messageid))
            await db.commit()
            return starcount
        else:
            starcount = 1
            await db.execute("INSERT INTO messages (guildid, channelid, messageid, starcount, userid) VALUES (?, ?, ?, ?, ?)", (guildid, channelid, messageid, starcount, userid))
            await db.commit()
            return starcount

async def del_reaction_DB(guildid, channelid, messageid): #unused function
    async with aiosqlite.connect("messages.db") as db:
        existingrecord = await db.execute("SELECT * FROM messages WHERE messageid=?", (messageid, ))
        existingrecord = await existingrecord.fetchone()
        if existingrecord:
            starcount = existingrecord[4] - 1
            await db.execute("UPDATE messages SET starcount=? WHERE messageid=? ", (starcount, messageid))
            await db.commit()
            return starcount
        else:
            starcount = 0
            await db.execute("INSERT INTO messages (guildid, channelid, messageid, starcount) VALUES (?, ?, ?, ?)", (guildid, channelid, messageid, starcount))
            await db.commit()
            return starcount
    
async def update_board_message(channel, starcount, message, emojiid, emojiname): #updates starboard message. Sometimes runs into race conditions
    embed = genEmbed(message)
    edit = 0
    async with aiosqlite.connect("messages.db") as db:
        record = await db.execute("SELECT * FROM messages WHERE messageid=?", (message.id,))
        record = await record.fetchone()
        #print(record[6])
        if record[6]:
            #print("edit")
            edit = 1
            
        if(emojiid == -1):
            boardmessagecontent = "**" + str(starcount) + "** " + emojiname + "  |  " + message.channel.mention
        else:
            boardmessagecontent = "**" + str(starcount) + "** <:" + emojiname + ":" + str(emojiid) + ">  |  " + message.channel.mention
            
    if(edit == 1):
        boardmessageid = record[6]
        #print("m3 " + str(boardmessageid))
        
        boardmessage = await channel.fetch_message(boardmessageid)
        await boardmessage.edit(content=boardmessagecontent, embed=embed)
    else:
        boardmessage = await channel.send(boardmessagecontent, embed=embed)
        
    async with aiosqlite.connect("messages.db") as db:
        await db.execute("UPDATE messages SET boardchannelid=?, boardmessageid=? WHERE messageid=? ", (boardmessage.channel.id, boardmessage.id, message.id))
        await db.commit()
    
    return boardmessage

async def del_board_message(boardmessage):#delete starboard message
    async with aiosqlite.connect("messages.db") as db:
        await db.execute("UPDATE messages SET boardchannelid=NULL, boardmessageid=NULL WHERE boardmessageid=?", (boardmessage.id, ))
        await db.commit()
    await boardmessage.delete()
    return
    
 
    
#(http)?s?:?(\/\/[^"']*\.(?:png|jpg|jpeg|gif|png|svg)).*
def gen_embed(message): #generator for starboard embeds.
    embed = discord.Embed(description=message.content)
    embed.color = discord.Colour(0xe6e600)
    embed.set_author(name=str(message.author.name + "#" + message.author.discriminator), icon_url=message.author.avatar_url)
    imageurl = re.search("(http)?s?:?(\/\/[^\"\']*\.(?:png|jpg|jpeg|gif|png|svg))(?:(?!\s).)*", message.content)
    for originembed in message.embeds:
        content = str(originembed.to_dict())
        #print(content)
        originurl = re.search("((http)?s?:?(\/\/[^\"\']*\.(?:png|jpg|jpeg|gif|png|svg)))(?:(?!\s).)*", content)
    try:
        equisde = originurl
    except UnboundLocalError:
        originurl = None
    if(originurl):
        embed.set_image(url=originurl.group(1))
    elif(imageurl):
        embed.set_image(url=imageurl.group())
    if(message.attachments):
        for att in message.attachments:
            attachmenturl = re.search("(http)?s?:?(\/\/[^\"\']*\.(?:png|jpg|jpeg|gif|png|svg))(?:(?!\s).)*", att.url)
            if(attachmenturl):
                embed.set_image(url=attachmenturl.group())
                break
    #embed.set_footer(text="Click to Jump", url=message.jump_url)
    embed.add_field(name="\u200b", value="**[Click to jump to message!](" + message.jump_url + ")**", inline=True)
    return embed
    
def bool_word(text):
    truewords = ["true", "1", "yes", "on", "enable"]
    falsewords = ["false", "0", "no", "off", "disable"]
    if(text in truewords):
        return True
    elif(text in falsewords):
        return False
    else:
        return None
